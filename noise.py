import numpy as np
import random
import math
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from functools import reduce

from matplotlib import rcParams
rcParams.update({'figure.autolayout': True})


experiment  = 2 #range {1,2} Change this flag to switch between experiment 1 and 2
maxval = 10
minval = 1

pnoise = 0.8 #in the geometric noise case, this is the probability that there is noise at all.

cutoffs = 0
noisedriftcounter=0
if experiment  == 1:
        cutoffsOnOff=False
        rounding=False
        normaldeviation=1
        gaussian=True
        n = 10**6 # num of agents
        tmax = 10*n # num of steps
        opinions = np.random.uniform(minval, maxval, size = n)

else:
        cutoffsOnOff = True
        rounding=True
        gaussian = False
        n = 10**3 # num of agents
        tmax = 10000*n # num of steps
        opinions= np.random.choice([8,9,10], size = n, p = [.0,.0,1]) #initial values



print(opinions)
currentSum=np.sum(opinions)
initialAverage=np.mean(opinions)
print ("Initial Average: {}".format(initialAverage))


if rounding == False:
    opinions = opinions.astype(float)

def compute_TSS():
    TSS = 0
    for i in range(0,n):
        TSS=TSS + (opinions[i]-initialAverage)**2
    return TSS
	
def cutoff(x):
    global cutoffs
    if cutoffsOnOff==True:
        if x > maxval or x < minval:
            cutoffs = cutoffs + 1
        return max(min(x,maxval),minval)
    else:
	    return x

def noise():
    if gaussian==True:
        return np.random.normal(0,normaldeviation)
    if random.random() < pnoise:
        return 0
    else:
        return (2 * random.randint(0,1) - 1) * np.random.geometric(pnoise)

def interaction(x,y):
    noise_x = noise()
    noise_y = noise()

    noisy_x = cutoff(x + noise_x)
    noisy_y = cutoff(y + noise_y)

    val1 = float(x + noisy_y) / 2.0
    val2 = float(noisy_x + y) / 2.0
    if rounding:
        return np.random.choice([math.ceil,math.floor])(val1), np.random.choice([math.ceil,math.floor])(val2)  #with rounding
    else:
        return float(val1), float(val2) #without rounding

def contribution(i,j):
    contr_i = (opinions[i]-initialAverage)**2
    contr_j = (opinions[j]-initialAverage)**2
    return contr_i + contr_j




TSS = compute_TSS()
TSSs = [TSS]
currentAverage=currentSum/n
phibar = TSS-(initialAverage-currentAverage)**2

phibars = [phibar]

averages = []
print('Initial Potential: {}'.format(phibar))
for t in range(tmax):
    i = random.randint(0,n-1)
    j = random.randint(0,n-1)
    if i == j:
        continue
    TSS=TSS-contribution(i,j)
    currentSum=currentSum-opinions[i]-opinions[j]
    opinions[i],opinions[j] = interaction(opinions[i],opinions[j])
    TSS=TSS+contribution(i,j)
    currentSum=currentSum+opinions[i]+opinions[j]
    currentAverage=float(currentSum)/float(n)
    phibar = TSS-(initialAverage-currentAverage)**2
    TSSs.append(TSS)
    phibars.append(phibar)
    averages.append(currentAverage)

    if t%(2**15)==0:
        print ("Potential at step {}: {}".format(t,phibar))
#distances to the mean
dist_to_mean = abs(opinions - currentAverage)

fig = plt.figure(tight_layout = True)
gs = gridspec.GridSpec(3,1)

ax = fig.add_subplot(gs[0,0])
plt.xlim((0,tmax))
ax.plot(averages)
ax.set_xlabel(r'time $t$')
ax.set_ylabel(r'Running Average $\varnothing(t)$')
ax = fig.add_subplot(gs[1,0])
ax.plot(phibars)
plt.xlim((0,tmax))
ax.set_xlabel(r'time $t$')
ax.set_ylabel(r'Potential $\bar\phi(t)$')
ax = fig.add_subplot(gs[2,0])
plt.xlim((0,np.max(dist_to_mean)))
if experiment == 1:
        ax.hist(dist_to_mean, bins=150, log=True, alpha = 0.5)
        ax.set_xlabel(r'Distance to Running Average $\varnothing^{(t)}$')
        ax.set_ylabel(r'$\log(count)$')
else:
        dist_to_mean = list(dist_to_mean)
        counts = [[x,dist_to_mean.count(x)] for x in set(dist_to_mean)]
        #print(dist_to_mean)
        counts.sort()
        #print(counts)
        ax.plot([c[0] for c in counts],[c[1] for c in counts],'bx',alpha=0.8)
        ax.set_xlabel(r'Distance to Running Average $\varnothing^{(t)}$')
        ax.set_ylabel(r'$count$')
fig.align_labels()


if experiment == 1:
	plt.savefig('ex1.png')
else:
	plt.savefig('ex2.png')
plt.show()

